import psutil as _psutil
import platform as _platform
from colorama import Fore as _Fore
from colorama import Style as _Style
from os import get_terminal_size
from time import sleep
# import distro


def bar(percent):
    c = get_terminal_size().columns - 2
    p_adj = round((percent/100) * c)
    w_spc = c - p_adj
    if percent <= 33:
        print(_Style.RESET_ALL + '[' + _Fore.LIGHTGREEN_EX + p_adj * '#'
              + _Style.RESET_ALL + w_spc * ' ' + ']')
    elif percent <= 66:
        print(_Style.RESET_ALL + '[' + _Fore.LIGHTYELLOW_EX + p_adj * '#'
              + _Style.RESET_ALL + w_spc * ' ' + ']')
    else:
        print(_Style.RESET_ALL + '[' + _Fore.LIGHTRED_EX + p_adj * '#'
              + _Style.RESET_ALL + w_spc * ' ' + ']')


class Convert():
    def humansize(self, arg):
        suffixes = ['B', 'KB', 'MB', 'GB', 'TB,', 'PB']
        i = 0
        while arg >= 1024 and i < len(suffixes)-1:
            arg /= 1024
            i += 1
        f = round(arg, 2)
        # :g remove insignificant trailing zeroes so we get 1KB instead 1.00KB
        return f'{f:g} {suffixes[i]}'


class SysInfo():
    def __init__(self):
        c = Convert()
        self.memTotal = c.humansize(_psutil.virtual_memory()[0])
        self.memAvailable = c.humansize(_psutil.virtual_memory()[1])
        self.memUsed = c.humansize(_psutil.virtual_memory()[3])
        self.memPercent = round((float(self.memUsed[:-3]) /
                                 float(self.memTotal[:-3]) * 100), 2)
        self.diskTotal = c.humansize(_psutil.disk_usage('/')[0])
        self.diskUsage = c.humansize(_psutil.disk_usage('/')[1])
        self.diskFree = c.humansize(_psutil.disk_usage('/')[2])
        self.diskFreePercent = c.humansize(_psutil.disk_usage('/')[3])
        self.cpuInfo = _platform.processor()
        self.cpuPercent = _psutil.cpu_percent()
        self.cpuLoadAvg = _psutil.getloadavg()
        self.cores = _psutil.cpu_count()
        self.netCount = _psutil.net_io_counters(pernic=False)
        self.netSent = c.humansize(self.netCount[0])
        self.netSentPack = self.netCount[2]
        self.netReceived = c.humansize(self.netCount[1])
        self.netReceivedPack = self.netCount[3]

    def system(self):
        # host = socket.gethostname()
        print(_Fore.LIGHTRED_EX +
              '\n-------------------- System --------------------\n'
              + _Style.RESET_ALL)
        print('OS                : ', _platform.system(), _platform.release(),
              _platform.machine())
        print('CPU               : ', self.cpuInfo, self.cores, 'Cores')
        print('Memory            : ', self.memTotal)
        print('Disk              : ', self.diskTotal)

    def cpu(self):
        print(_Fore.LIGHTRED_EX +
              '\n-------------------- CPU --------------------\n'
              + _Style.RESET_ALL)
        print('CPU usage         : ', self.cpuPercent, "%")
        print('CPU               : ', self.cpuInfo, self.cores, ' Cores')
        # create proportional status bar
        bar(self.cpuPercent)

        print(_Fore.LIGHTRED_EX +
              '\n-------------------- Load Avarage -------------------\n'
              + _Style.RESET_ALL)
        print('Load avg, 1 min   : ', round(self.cpuLoadAvg[0], 2))
        print('Load avg, 5 min   : ', round(self.cpuLoadAvg[1], 2))
        print('Load avg, 15 min  : ', round(self.cpuLoadAvg[2], 2))

    def memory(self):
        print(_Fore.LIGHTRED_EX +
              '\n-------------------- Memory --------------------\n'
              + _Style.RESET_ALL)
        print('Total Memory      : ', self.memTotal)
        print('RAM Used          : ', self.memUsed, '-', self.memPercent, '%')
        print('RAM Available     : ', self.memAvailable)
        bar(self.memPercent)

    def network(self):
        print(_Fore.LIGHTRED_EX +
              '\n-------------------- Network --------------------\n'
              + _Style.RESET_ALL)
        print('Total send        : ', self.netSent, '-',
              self.netSentPack, 'Packets')
        print('Total recv        : ', self.netReceived, '-',
              self.netReceivedPack, 'Packets')

    def main(self):
        "print all results"
        self.system()
        self.cpu()
        self.memory()
        self.network()

if __name__ == '__main__':
    while True:
        main = SysInfo()
        main.main()
        sleep(5)

        LINE_UP = 31 * '\033[1A'
        LINE_CLEAR = 31 * '\x1b[2K'
        print(LINE_UP, end=LINE_CLEAR)
    # main = SysInfo()
    # main.main()
